# csuchen-Guard

Automatically remove the cheaters' artikels on csuchen.de 

## windows:

- 批处理bat文件内容：
- cmd 中输入 python 目录\main.py 版主用户名 密码
- 注意密码如果含有\^等特殊字符, 在cmd 和 bat 文件中需要输入两次
- 直接运行csuchen_defender.vbs脚本,设为开机自动启动即可循环,时间间隔设置为10000*2 毫秒,注意修改相应文件中main的目录

## Linux:

- 修改脚本 autorun.sh 版主用户名密码
- 运行脚本 ./autorun.sh

## 黑名单:

- config.ini文件中写入骗子的uid, 用户名可以随意写,uid需要到csuchen相应用户页面查询
- 添加不存在uid有可能出现误删帖！
