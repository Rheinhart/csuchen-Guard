__author__ = 'taojun'
# -*- coding: utf-8 -*-
import sys
import re
import datetime
from bs4 import BeautifulSoup
import ConfigParser
import requests
import os

######
reload(sys)
sys.setdefaultencoding("utf8")
######
login_url = 'http://www.csuchen.de/bbs/logging.php?action=login&loginsubmit=yes&inajax=1'
login_session = requests.Session()

class csuchen():
    """Automatically login"""
    def __init__(self,refresh=15):

        self.username = ''
        self.password = ''
        self.refreshTime = refresh
        self.uid = []

    def setInfo(self,username,password):
        '''set the user information'''
        self.username = username
        self.password = password

    def __loginmain(self):
        '''login the main page'''

        login_header = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                  'Accept-Encoding':'gzip, deflate',
                  'Accept-Language':'en-US,en;q=0.5',
                  'Connection':'keep-alive',
                  'Content-Type':'application/x-www-form-urlencoded',
                  'DNT':'1',
                  'Host':'csuchen.de',
                  'Origin':'www.csuchen.de',
                  'Referer':'http://www.csuchen.de/bbs/logging.php?action=login',
                  'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0'}


        postData = {'formhash':'34a2028c',
                    'referer':'index.php',
                    'loginfield':'username',
                    'username':self.username,
                    'password':self.password,
                    'questionid':0,
                    'answer':'',
                    'cookietime':2592000
                    }

        req = login_session.post(login_url, postData,headers=login_header)

        if  re.search(self.username,req.content):
            #login successful
            print 'Login success!'
        else:
            #login failure
            print 'Login failed'

    def __parser(self,page,uid):
        try:
            soup = BeautifulSoup(page)
            regex='viewthread.php\?tid=(.*)\&extra=page\%3D1'
            r = []
            articles = []

            for link in soup.find_all('a'):
                r.append(link.get('href'))
            for i in range(len(r)-1):
                if uid in r[i]:
                    if r[i-1]:
                        id = re.findall(regex,r[i-1])
                        if id:
                            articles.append(id[0])
            return articles
        except Exception, e:
            print 'Error'

    def __delete(self,uid):

        header = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                  'Accept-Encoding':'gzip, deflate',
                  'Accept-Language':'en-US,en;q=0.5',
                  'Content-Type':'application/x-www-form-urlencoded',
                  'Content-Length':160,
                  'Connection':'keep-alive',
                  'Host':'csuchen.de',
                  'DNT': '1',
                  'Origin': 'http://csuchen.de',
                  'Upgrade-Insecure-Requests':1,
                  'Referer':'http://csuchen.de/bbs/forumdisplay.php?fid=24&page=1',
                  'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'}

        getdata_url='http://www.csuchen.de/bbs/forumdisplay.php?fid=24&page=1'
        req = login_session.get(getdata_url)
        page= req.content
        try:
            for userid in uid:
                userid = str(userid)
                articles=self.__parser(page,userid)
                for articleId in articles:
                    print 'article id is:'+articleId
                    postData = {'frommodcp':0,
                                'formhash':'8f5b1dce',
                                'fid':24,
                                'listextra':'page%3D1',
                                'handlekey':'mods',
                                'moderate[]':articleId,
                                'operations[]':'delete',
                                'reason':'',}

                    url = 'http://csuchen.de/bbs/topicadmin.php?action=moderate&optgroup=3&modsubmit=yes&infloat=yes&inajax=1'
                    req = login_session.post(url, postData,headers=header)

                    tag = 'return_mods'
                    if tag in req.content:
                    	self.log(userid,articleId)
                        print 'delete successfully\n'
                    else:
                        print 'can not delete\n'

        except Exception, e:
            print 'delete error'


    def log(self,uid,articleId):

        data = datetime.date.today().strftime("%Y-%m-%d")
        logdir = sys.path[0]+'/logs/'
        if not os.path.exists(logdir):
            os.makedirs(logdir)
        dlog = logdir+data+'.log'

    	time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    	info = time+' uid:'+uid+' article_id:'+str(articleId)+' Deleted\n'
        try:
            log=open(dlog,'a+')
            log.writelines(info)
            log.close()

        except IOError:
            print 'log failure!'


    def config(self,cfile):
    	try:
            config=ConfigParser.SafeConfigParser()
            config.read(cfile)
            cheaters=config.options('Cheaters')

            for cheater in cheaters:
                id=config.get('Cheaters',cheater)
                self.uid.append(id)

        except Exception, e:
            print 'Reading config.ini error'

    def protection(self):

        self.__loginmain()
        self.__delete(self.uid)
        login_session.close()

if __name__ == '__main__':

    name = sys.argv[1]
    pwd = sys.argv[2]

    dwork = csuchen()
    dwork.setInfo(name,pwd)
    cfile = sys.path[0] + '/config.ini'
    print(cfile)
    print(pwd)
    dwork.config(cfile)
    dwork.protection()