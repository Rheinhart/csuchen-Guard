#!/bin/bash
#Created by taojun
#For Linux Server, Guard Server monitor

USERNAME=""
PASSWORD=""
Guard_DIR=$(pwd)
SLEEP_TIME=30
while :
    
  do
        echo "Guard restarting...:"
        cd ${Guard_DIR}
        nohup python main.py ${USERNAME} ${PASSWORD}>/dev/null 2>&1 &
        sleep ${SLEEP_TIME}
        continue
    echo ""
done
